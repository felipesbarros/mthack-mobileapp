import { Component } from '@stencil/core';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css'
})
export class AppHome {

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-title>Home</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content padding>
        <ion-item>

              Seja bem-vindo!

                Digite abaixo o seu celular com DDD para interagir com a sua embalagem :)! Uhul!

        </ion-item>
        <ion-item>
          <ion-input required type="number" placeholder="Telefone"></ion-input>
        </ion-item>
        <ion-button href="/profile/ionic" expand="full" shape="round" >Iniciar</ion-button>
      </ion-content>
    ];
  }
}
